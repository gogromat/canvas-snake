canvas-snake
============

HTML5 Snake game made using Canvas and JavaScript.

![Snake](snake.png)

How to make my own
============
[Here][1] is the online tutorial on how to make a snake game
using canvas & javascript (uses jQuery).

demo
============
[Here][demo] is the demo of the game.

dependencies
============
[lo-dash][lodash]

[1]: http://thecodeplayer.com/walkthrough/html5-game-tutorial-make-a-snake-game-using-html5-canvas-jquery
[lodash]: lodash.com
[demo]: http://jsfiddle.net/gogromat/5FfP4/