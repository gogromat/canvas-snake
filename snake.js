document.addEventListener('DOMContentLoaded', function () {
    
    var game = document,
        gameId,
        
        pauseButton = document.querySelector("#pause"),
        speedIncrease = document.querySelector("input[name=increaseSpeedEachNFoods]"),
        initialSpeedSlide = document.querySelector("input[name=initialSpeed]"),
        scoreResult = document.querySelector(".score-counter"),
        score = 0,
        
        // CANVAS related
        canvas = document.querySelector("#canvas"),
        ctx = canvas.getContext("2d"),
        
        w = canvas.clientWidth,
        h = canvas.clientHeight,
        
        // CELL related
        cw = 20,
        ch = 20,
        
        // SNAKE relaled
        snakeBeginningLength = 4,
        snake = [],
        
        // FOOD related
        food = {},
        eattenFood = [],
        grow_next_iteration = false,
        foodOrder = [],
        
        // DIRECTION related
        DIRECTIONS = {
            RIGHT: {x:1,  y:0},
            LEFT:  {x:-1, y:0},
            DOWN:  {x:0,  y:1},
            UP:    {x:0,  y:-1}
        },
        currentDirection = DIRECTIONS.RIGHT,
        
        // GAME loop
        gameLoop,
        is_paused = true,
        is_ended  = true,

        // SPEED related
        lastLoopTime = 0,
        speed = 0,
        speedScoreInrement = 0,


    
        // RANDOM (for food)
        getRandom = function (min, max) {
            //     guar. whole  [0,1)                guar. max   guar. min 
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        getRandomWidth = function () {
            return getRandom(0, (w / cw) - 1);
        },
        getRandomHeight = function () {
            return getRandom(0, (h / ch) - 1);
        };

    // used to test snake game
    function test_matrix () {
        var horizontal = w / cw,
            vertical   = h / ch,
            result = "\n\r____________\n\r";
        for (var j = 0; j < horizontal; j++) {
            result += "|";
            for (var v = 0; v < vertical; v++) {
                if (food.x && food.x == v && food.y == j) {
                    result += "x";
                } else if (_.find(eattenFood, {x:v, y:j})) {
                    result += "*";
                } else if (_.find(snake, {x:v, y:j})) { 
                    result += "o";
                } else {
                    result += " ";
                }
            }
            result += "|\n\r";
        }
        result += "____________\n\r";
        console.log(result, "\n\r", eattenFood.length);
    }

    function create_snake() {
        /* T [length]
           H [0]       pop() removes tail */
        for (var i = snakeBeginningLength; i > 0; i--) {
            snake.push({x: i, y: 1});
        }
    }
    
    function move_snake(direction) {
        // take current tail, put before head
        var newHead = snake.pop(),
            currentHead = snake[0];
        newHead.x = currentHead.x + direction.x;
        newHead.y = currentHead.y + direction.y;
        snake.unshift(newHead);
    }
    
    function grow_snake() {
        // push oldest food on tail of snake
        snake.push(eattenFood.pop());
    }
    
    function feed_snake(food) {
        // put new food at bottom of the stack
        eattenFood.unshift(_.extend({}, food));
        score++;
        if (speedScoreInrement && !(score % speedScoreInrement)) {
            speed -= 10;
        }
    }
    
    function snake_hit_wall() {
        var head = snake[0];
        return (head.y < 0 || head.y >= (h / ch) || 
                head.x < 0 || head.x >= (w / cw)); 
    }
    
    function snake_hit_food() {
        var head = snake[0];
        return (head.x == food.x && head.y == food.y);
    }
    
    function food_in_snake_at_tail() {
        var tail = snake[snake.length -1],
            bottomFood = eattenFood[eattenFood.length-1];
        return (bottomFood && 
            bottomFood.x == tail.x && bottomFood.y == tail.y);
    }
    
    function snake_hit_itself() {
        // finds if snake's head repeats in rest of the snake
        return (_.find(snake.slice(1), snake[0]));
    }
    
    function clear_canvas() {
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, w, h);
    }
    
    function draw_snake() {
        var snakeLength = snake.length;
        for (var i=0; i < snakeLength; i++) {
            var snakePart = snake[i];
            draw_tile(snakePart, 
                      (i == 0 ? "red" : "green"), "white", 
                      (i == 0 ? "circle" : "rectangle"));
        }
    }
    
    function draw_food() {
        if (_.isEmpty(food)) {
            food = {
                // new food position
                x: getRandomWidth(),
                y: getRandomHeight()
            };  
        } 
        if (_.find(snake, food)) {
            // cannot put on snake, make new food
            make_new_food();
            draw_food();
        } else {
            draw_tile(food, "blue", "white");
        }
    }
    
    function make_new_food() {
        food = {};
    }
    
    function draw_tile(tile, color, strokeColor, figure) {
        ctx.fillStyle = color;
        ctx.fillRect(tile.x * cw, tile.y * ch, cw, ch);
        if (strokeColor) {
            ctx.strokeStyle = strokeColor;
            ctx.strokeRect(tile.x * cw, tile.y * ch, cw, ch);
        }
    }
    
    function gameLoop(time) {

        if (time - lastLoopTime > speed) {
            paint();
            lastLoopTime = time;
        }

        if (gameId) {        
            // next loop
            requestAnimationFrame(gameLoop);
        }
    }

    function cancelGameLoop() {
        cancelAnimationFrame(gameLoop);
        gameId = null;
    }

    function paint() {
        clear_canvas();
        draw_snake();
        draw_food();
        move_snake(currentDirection);
        
        if (snake_hit_wall() || snake_hit_itself()) {
            end_game();
        }
        
        if (grow_next_iteration) {
            // food was at the tail, grow snake
            grow_next_iteration = false;
            grow_snake();
        }
        if (food_in_snake_at_tail()) {
            foodOrder.push(food);
            // food at the tail of the snake, grow next cycle
            grow_next_iteration = true;
            //grow_snake();
        }
        
        if (snake_hit_food()) {
            // add food to the snake's belly and make new food
            feed_snake(food);
        }
        write_score();
    }
    
    function going_reverse(newDirection) {
        // checks if new direction is the reverse of current one
        return ((currentDirection.x + newDirection.x) == 0 &&
                (currentDirection.y + newDirection.y) == 0)
    }
       
    // Key events - changes snake direction
    game.onkeydown = function (e) {
        var key = e.keyCode;
               if (key == 37 && !going_reverse(DIRECTIONS.LEFT))  {  
            currentDirection = DIRECTIONS.LEFT;
        } else if (key == 38 && !going_reverse(DIRECTIONS.UP))    {    
            currentDirection = DIRECTIONS.UP;
        } else if (key == 39 && !going_reverse(DIRECTIONS.RIGHT)) { 
            currentDirection = DIRECTIONS.RIGHT;
        } else if (key == 40 && !going_reverse(DIRECTIONS.DOWN))  {  
            currentDirection = DIRECTIONS.DOWN;
        } else if (key == 78 || key == 80 || key == 82 || key == 32) { // n, p, r, space
            start_pause_resume();
        }
    }
    // Control button
    pauseButton.onclick = function () {
        start_pause_resume();
    }
    
    // Changes classes of control button and call start/pause/resume on game
    function start_pause_resume() {
        if (is_ended) {
            pauseButton.classList.remove("paused");
            pauseButton.classList.remove("new");
        } else {
            pauseButton.classList.toggle("paused");
        }
        start_pause_resume_game();
    }
    
    // Starts, pauses or resumes a game
    function start_pause_resume_game() {
        if (is_ended) {
            start_game();
        } else {
            if (is_paused) {
                console.log("Game Resumed");
                start_game();
            } else {
                console.log("Game Paused");
                is_paused = true;
                //clearInterval(gameLoop);
                cancelGameLoop();
            }
        }
    }
    
    function start_game() {
        if (!is_ended) {
            gameId = requestAnimationFrame(gameLoop);
        } else {
            // reset values back to their defaults,
            // and call the function again
            score = 0;
            snake = [];
            create_snake();
            is_ended = false;
            is_paused = false;
            currentDirection = DIRECTIONS.RIGHT;
            
            lastloopTime = 0;
            speed = getSpeed();
            speedScoreInrement = getSpeedStep();

            make_new_food();
            eattenFood = [];
            
            start_game();
        }
    }
    
    function end_game() {
        // console.log("%c GAME ENDED", "background-color: yellow");
        is_ended = true;
        
        cancelGameLoop();

        pauseButton.classList.add("new");
        if (won()) {
            draw_semi_opaque_text("You won! Final score: " + score + ".", null, "green");
        } else {
            draw_semi_opaque_text("You lost, final score: " + score + ".", null, "red");
        }
    }
    
    draw_semi_opaque_text(["Welcome to the Snake game!",
                           "Click on \"new game\" to begin!"],
                           null, "green");
    
    function draw_semi_opaque_text(text, font, color) {
        ctx.fillStyle = "rgba(255,255,255,0.7)"; 
        ctx.fillRect(0,0,w,h);
        
        ctx.fillStyle = color || "red";
        ctx.font = font || "bold 18px sans-serif";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        
        if (text.length && text.forEach) {
            text.forEach(function(t, i) {
                // 18 px + 7 padding
                fillText(t, w/2, h/2 + (i * 25));
            });
        } else {
            fillText(text, w/2, h/2);
        }
    }
                           
    function fillText(text, x, y) {
        ctx.fillText(text, x, y);
    }
    
    function won() {
        return score == ((w / cw) * (h / ch)) - snakeBeginningLength; 
    }
    
    function write_score() {
        scoreResult.innerHTML = score;
    }
    
    function getSpeedStep() {
        return speedIncrease.value;
    }

    function getSpeed() {
        var max = initialSpeedSlide.getAttribute('max'),
            min = initialSpeedSlide.getAttribute('min'),
            middle = max / 2, 
            value = initialSpeedSlide.value;

        if (value < middle) return max - value;
        else if (value > middle) return Math.abs(value - max);
        else return value; 
    }

});